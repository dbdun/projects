#include <fauxmoESP.h>
#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <WiFiUdp.h>
#include <functional>
#include "switch.h"
#include "UpnpBroadcastResponder.h"
#include "CallbackFunction.h"


//final working code for Nodemcu v3 and compatible for 8 Channel Power Relay..
//fb.com/insider7enjoy
//programmed on 13-Sep-2017
//PhantomCluster.com

// prototypes
boolean connectWifi();

//on/off callbacks 
void hotTubROn();
void hotTubROff();
void hotTubLOn();
void hotTubLOff();
void grillROn();
void grillROff();
void grillLOn();
void grillLOff();
void frontYardROn();
void frontYardROff();
void fronYardLOn();
void frontYardLOff();
void shopROn();
void shopROff();
void shopLOn();
void shopLOff();

// Change this before you flash
const char* ssid = "Wirethis";
const char* password = "unplugger";

boolean wifiConnected = false;

UpnpBroadcastResponder upnpBroadcastResponder;

Switch *hotTubR = NULL;
Switch *hotTubL = NULL;
Switch *grillR = NULL;
Switch *grillL = NULL;
Switch *frontYardR = NULL;
Switch *frontYardL = NULL;
Switch *shopR = NULL;
Switch *shopL = NULL;

//relay pin setup for funct
int relayOne = 5;
int relayTwo = 4;
int relayThree = 0;
int relayFour = 14;
int relayFive = 12;
int relaySix = 13;
int relaySeven = 15;
int relayEight = 3;


void setup()
{
 
 Serial.begin(115200);
   
  // Initialise wifi connection
  wifiConnected = connectWifi();
  
  if(wifiConnected){
    upnpBroadcastResponder.beginUdpMulticast();
    
    // Define your switches here. Max 14
    // Format: Alexa invocation name, local port no, on callback, off callback
    hotTubR = new Switch("Hot Tub Right", 80, hotTubROn, hotTubROff);
    hotTubL = new Switch("Hot Tub Left", 81, hotTubLOn, hotTubLOff);
    grillR = new Switch("Grill Right", 82, grillROn, grillROff);
    grillL = new Switch("Grill Left", 83, grillLOn, grillLOff);
    frontYardR = new Switch("Front Yard Right", 84,frontYardROn, frontYardROff);
    frontYardL = new Switch("Front Yard Left", 85, frontYardLOn, frontYardLOff);
    shopR = new Switch("Shop Right", 86, shopROn, shopROff);
    shopL = new Switch("Shop Left", 87,shopLOn, shopLOff);

    Serial.println("Adding switches upnp broadcast responder");
    upnpBroadcastResponder.addDevice(*hotTubR);
    upnpBroadcastResponder.addDevice(*hotTubL);
    upnpBroadcastResponder.addDevice(*grillR);
    upnpBroadcastResponder.addDevice(*grillL);    
    upnpBroadcastResponder.addDevice(*frontYardR);
    upnpBroadcastResponder.addDevice(*frontYardL);
    upnpBroadcastResponder.addDevice(*shopR);
    upnpBroadcastResponder.addDevice(*shopL);
    
    //relay pins setup i Used D1,D2,D3,D4,D5,D6,D7,D8 followed as assigned below, if you are willing to change Pin or planning to use extra please Check Image in Github File..:)
    pinMode (5, OUTPUT);
    pinMode (4, OUTPUT);
    pinMode (0, OUTPUT);
    pinMode (2, OUTPUT);
    pinMode (14, OUTPUT);
    pinMode (12, OUTPUT);
    pinMode (13, OUTPUT);
    pinMode (15, OUTPUT);
    digitalWrite (5,HIGH);
    digitalWrite (4,HIGH);
    digitalWrite (0,HIGH);
    digitalWrite (2,HIGH);
    digitalWrite (14,HIGH);
    digitalWrite (12,HIGH);
    digitalWrite (13,HIGH);
    digitalWrite (15,HIGH);
  }
}
 
void loop()
{
	 if(wifiConnected){
      upnpBroadcastResponder.serverLoop();
      
      hotTubR->serverLoop();
      hotTubL->serverLoop();
      grillR->serverLoop();
      grillL->serverLoop();
      frontYardR->serverLoop();
      frontYardL->serverLoop();
      shopR->serverLoop();
      shopL->serverLoop();
	 }
}

void hotTubROff() {
    Serial.print("Hot Tub Right 1 turn off ...");
    digitalWrite(relayOne, HIGH);   // sets relayOne off
}

void hotTubROn() {
    Serial.print("Hot Tub Right 1 turn on ...");
    digitalWrite(relayOne, LOW);   // sets relayOne on
}

void hotTubLOff() {
    Serial.print("Hot Tub Left 1 turn off ...");
    digitalWrite(relayTwo, HIGH);   // sets relayTwo off
}

void hotTubLOn() {
  Serial.print("Hot Tub Left 1 turn on ...");
  digitalWrite(relayTwo, LOW);   // sets relayTwo on
}

void grillROff() {
    Serial.print("Grill Right 1 turn off ...");
    digitalWrite(relayThree, HIGH);   // sets relayThree on
}

void grillROn() {
  Serial.print("Grill Right 1 turn on ...");
  digitalWrite(relayThree, LOW);   // sets relayThree on
}

void grillLOff() {
    Serial.print("Grill Left 1 turn off ...");
    digitalWrite(relayFour, HIGH);   // sets relayFour on
}

void grillLOn() {
  Serial.print("Grill Left 1 turn on ...");
  digitalWrite(relayFour, LOW);   // sets relayFour on
}

void frontYardROff() {
    Serial.print("Front Yard Right 1 turn off ...");
    digitalWrite(relayFive, HIGH);   // sets relayFive on
}

void frontYardROn() {
    Serial.print("Front Yard Right 1 turn on ...");
    digitalWrite(relayFive, LOW);   // sets relayFive off
}

void frontYardLOff() {
    Serial.print("Front Yard Left 1 turn off ...");
    digitalWrite(relaySix, HIGH);   // sets relaySix on
}

void frontYardLOn() {
  Serial.print("Front Yard Left 1 turn on ...");
  digitalWrite(relaySix, LOW);   // sets relaySix on
}

void shopROff() {
    Serial.print("Shop Right 1 turn off ...");
    digitalWrite(relaySeven, HIGH);   // sets relaySeven on
}

void shopROn() {
    Serial.print("Shop Right 1 turn on ...");
    digitalWrite(relaySeven, LOW);   // sets relaySeven off
}

void shopLOff() {
    Serial.print("Shop Left 1 turn off ...");
    digitalWrite(relayEight, HIGH);   // sets relayEight on
}

void shopLOn() {
  Serial.print("Shop Left 1 turn on ...");
  digitalWrite(relayEight, LOW);   // sets relayEight on
}

// connect to wifi – returns true if successful or false if not
boolean connectWifi(){
  boolean state = true;
  int i = 0;
  
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  Serial.println("");
  Serial.println("Connecting to WiFi");

  // Wait for connection
  Serial.print("Connecting ...");
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
    if (i > 10){
      state = false;
      break;
    }
    i++;
  }
  
  if (state){
    Serial.println("");
    Serial.print("Connected to ");
    Serial.println(ssid);
    Serial.print("IP address: ");
    Serial.println(WiFi.localIP());
  }
  else {
    Serial.println("");
    Serial.println("Connection failed.");
  }
  
  return state;
}
